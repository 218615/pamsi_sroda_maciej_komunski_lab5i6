#include <iostream>
#include <cmath>
#include <cstdlib>
#include <chrono>


#include "Tablica.hpp"
#include "Sort.hpp"
using namespace std;
using namespace chrono;


class Stoper{
public:

  time_point<system_clock> start, end,elapsed_seconds;
  time_point<system_clock> &tmpS=start;
  time_point<system_clock> &tmpE=end;
  void zacznij();
  void koniec();
  void get_time();
};

void Stoper:: zacznij(){
  tmpS= system_clock::now();
}

void Stoper::koniec(){
  
 tmpE = system_clock::now();
 
}

void Stoper::get_time(){

duration<double> elapsed_seconds = tmpE-tmpS;
cout << "czas trwania programu:"<< elapsed_seconds.count() << "s\n";
}

int main() {
	srand(time( NULL));   // resetowanie zegara


	
	int n = 0;
	int wybor = 0;

	cout << "podaj ilosc:" << endl;
	cin >> n;
	Stoper S;
	TabSort<int> dane(n);

	while (wybor != 4) {
		cout << "1.Wszystkie elementy tablicy losowe." << endl
				<< "2.Procentowo posortowane." << endl
				<< "3.Wszystkie elementy posortowane odwrotnie." << endl
				<< "4.Exit." << endl;
		cin >> wybor;

		switch (wybor) {
		case 1:
/////////////////////////////////////////////////////////////////////////////
			cout << "Sortshell" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
		        S.zacznij();
				sortshell(dane, n);
		          S.koniec();

			}
			S.get_time();
		        
//////////////////////////////////////////////////////////////////////////////
			cout << "Heap sort" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
		         S.zacznij();
				sortheap(dane, n);
		          S.koniec();
  
			}
			S.get_time();
	        
//////////////////////////////////////////////////////////////////////////////

			cout << "Scalanie" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
		         S.zacznij();
				scalanie_sort(dane, 0, (n - 1));
		          S.koniec();
		        
			}
			S.get_time();
	        
///////////////////////////////////////////////////////////////////////////////
			cout << "Quicksort" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
				 S.zacznij();
				quicksort(dane, 0, (n - 1));
		        
		         S.koniec();
			}
S.get_time();
       
///////////////////////////////////////////////////////////////////////////////
			break;

		case 2:
			int wybor1, wypelnienie;
			while (wybor1 != 7) {
				cout << "1.Posortowane 25%." << endl << "2.Posortowane 50%."
						<< endl << "3.Posortowane 75%." << endl
						<< "4.Posortowane 95%." << endl << "5.Posortowane 99%."
						<< endl << "6.Posortowane 99.7%." << endl << "7.Koniec."
						<< endl;

				cin >> wybor1;
				switch (wybor1) {
				case 1:
					wypelnienie = (n * 0.25);

					/////////////////////////////////////////////////////////////////////////////
					cout << "Sortshell" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						sortshell(dane, n);
				        
				         S.koniec();
					}
					S.get_time();
			        
					//////////////////////////////////////////////////////////////////////////////
					cout << "Heap sort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						sortheap(dane, n);
					          
				         S.koniec();
					}

			        S.get_time();
					//////////////////////////////////////////////////////////////////////////////

					cout << "Scalanie" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						scalanie_sort(dane, 0, (n - 1));
				          S.koniec();
					}
					S.get_time();
				        

					///////////////////////////////////////////////////////////////////////////////
					cout << "Quicksort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						quicksort(dane, 0, (n - 1));
					      S.koniec();   
					}

				        S.get_time();
					///////////////////////////////////////////////////////////////////////////////
					break;
				case 2:
					wypelnienie = (n * 0.5);

					/////////////////////////////////////////////////////////////////////////////
					cout << "Sortshell" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						sortshell(dane, n);
					          S.koniec();
					}

					S.get_time();
					//////////////////////////////////////////////////////////////////////////////
					cout << "Heap sort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						sortheap(dane, n);
					         S.koniec();
					}

			        
					//////////////////////////////////////////////////////////////////////////////

					cout << "Scalanie" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						scalanie_sort(dane, 0, (n - 1));
				           S.koniec();
					}

			        
					S.get_time();
					///////////////////////////////////////////////////////////////////////////////
					cout << "Quicksort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						quicksort(dane, 0, (n - 1));
					         S.koniec();
					}

			        S.get_time();
					///////////////////////////////////////////////////////////////////////////////
					break;
				case 3:
					wypelnienie = (n * 0.75);

					/////////////////////////////////////////////////////////////////////////////
					cout << "Sortshell" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						sortshell(dane, n);
					         S.koniec();
					}

				        S.get_time();
					//////////////////////////////////////////////////////////////////////////////
					cout << "Heap sort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						sortheap(dane, n);
					         S.koniec();
					}
					S.get_time();
			        
					//////////////////////////////////////////////////////////////////////////////

					cout << "Scalanie" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						scalanie_sort(dane, 0, (n - 1));
				        S.koniec();
					}
					S.get_time();
			        

					///////////////////////////////////////////////////////////////////////////////
					cout << "Quicksort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						quicksort(dane, 0, (n - 1));
					         S.koniec();
					}
					S.get_time();
			        
					///////////////////////////////////////////////////////////////////////////////
					break;
				case 4:
					wypelnienie = (n * 0.95);

					/////////////////////////////////////////////////////////////////////////////
					cout << "Sortshell" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						sortshell(dane, n);
				         S.koniec();
					}
					S.get_time();
				        
					//////////////////////////////////////////////////////////////////////////////
					cout << "Heap sort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						sortheap(dane, n);
						
				         S.koniec();
					}

				        S.get_time();
					//////////////////////////////////////////////////////////////////////////////

					cout << "Scalanie" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
				         S.zacznij();
						scalanie_sort(dane, 0, (n - 1));
					         S.koniec();
					}
					S.get_time();
					///////////////////////////////////////////////////////////////////////////////
					cout << "Quicksort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						quicksort(dane, 0, (n - 1));
				         S.koniec();
					}
					S.get_time();
			        
					///////////////////////////////////////////////////////////////////////////////
					break;
				case 5:
					wypelnienie = (n * 0.99);

					/////////////////////////////////////////////////////////////////////////////
					cout << "Sortshell" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						sortshell(dane, n);
				         S.koniec();
					}

					S.get_time();
					//////////////////////////////////////////////////////////////////////////////
					cout << "Heap sort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					        S.zacznij();
						sortheap(dane, n);
						S.koniec();
					}
					S.get_time();
				        
					//////////////////////////////////////////////////////////////////////////////

					cout << "Scalanie" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
					         S.zacznij();
						scalanie_sort(dane, 0, (n - 1));
						S.koniec();
					}
					S.get_time();
			        
					///////////////////////////////////////////////////////////////////////////////
					cout << "Quicksort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						S.zacznij();
						quicksort(dane, 0, (n - 1));
				        
					}
					S.get_time();
					///////////////////////////////////////////////////////////////////////////////
					break;
				case 6:
					wypelnienie = (n * 0.997);

					/////////////////////////////////////////////////////////////////////////////
					cout << "Sortshell" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						//	QueryPerformanceCounter(&t1);
						sortshell(dane, n);
				         S.koniec();
					}
					S.get_time();
				        
					//////////////////////////////////////////////////////////////////////////////
					cout << "Heap sort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						sortheap(dane, n);
				         S.koniec();
					}
					S.get_time();
			        
					//////////////////////////////////////////////////////////////////////////////

					cout << "Scalanie" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						scalanie_sort(dane, 0, (n - 1));
				         S.koniec();
					}
					S.get_time();
			        
					///////////////////////////////////////////////////////////////////////////////
					cout << "Quicksort" << endl << endl;
					for (int i = 0; i < 100; ++i) {
						dane.wypelnij();
						quicksort(dane, 0, wypelnienie - 1);
						 S.zacznij();
						quicksort(dane, 0, (n - 1));
					        S.koniec(); 
					}
					S.get_time();
		        
					///////////////////////////////////////////////////////////////////////////////
					break;
				case 7:
					break;
				default:
					break;
				}

			}
			break;

		case 3:
			/////////////////////////////////////////////////////////////////////////////
			cout << "Sortshell" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
				quicksort(dane, 0, (n - 1));
				dane.invers();
				 S.zacznij();
				sortshell(dane, n);
			        S.koniec(); 
			}
			S.get_time();

			//////////////////////////////////////////////////////////////////////////////
			cout << "Heap sort" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
				quicksort(dane, 0, (n - 1));
				dane.invers();
				S.zacznij();
				sortheap(dane, n);
		        S.koniec(); 
			        
			}
			S.get_time();


			cout << "Scalanie" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
				quicksort(dane, 0, (n - 1));
				dane.invers();
				 S.zacznij();
				scalanie_sort(dane, 0, (n - 1));
			       S.koniec(); 
			}
			S.get_time();

			///////////////////////////////////////////////////////////////////////////////
			cout << "Quicksort" << endl << endl;
			for (int i = 0; i < 100; ++i) {
				dane.wypelnij();
				quicksort(dane, 0, (n - 1));
				dane.invers();
				 S.zacznij();
				quicksort(dane, 0, (n - 1));
		        S.koniec(); 
			}
			S.get_time();


			break;

		case 4:
			break;

		default:
			break;
		}
	}
	return 0;
}
