#ifndef TABLICA_HPP_
#define TABLICA_HPP_
#include <iostream>

using namespace std;

template<typename typ>

class TabSort {

	int ilosc;
	typ* tablica;

public:

	//konstruktor
	TabSort(int n) {
		tablica = new typ[n];
		ilosc = n;
	}

	// odczyt tablicy
	typ operator[](int a) const {
		return tablica[a];
	}

	// zapis tablicy
	typ& operator[](int a) {
		return tablica[a];
	}
	// wypelnianie liczbami losowymi
	void wypelnij() {
		for (int i = 0; i < ilosc; ++i) {
			tablica[i] = rand() % 100000000;
		}
	}
	// wyswietlenie
	void wyswietl() {
		cout << "Zawartosc:" << endl;
		for (int i = 0; i < ilosc; i++) {
			cout << tablica[i] << endl;
		}
	}
	//odwróc
	void invers() {
		typ* tmp = new typ[ilosc];

		for (int i = 0; i < ilosc; ++i) {
			tmp[i] = tablica[ilosc - 1 - i];
		}
		tablica = tmp;
	}

};

#endif /* TABLICA_HPP_ */
