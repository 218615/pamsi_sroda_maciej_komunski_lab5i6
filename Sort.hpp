#ifndef SORT_HPP_
#define SORT_HPP_
#include "Tablica.hpp"
template<typename typ> void scalanie_sort(TabSort<typ> tab, const int pierwszy,
		const int ostatni) {
	if (pierwszy >= ostatni)
		return;
	int mid = (pierwszy + ostatni) / 2;
	scalanie_sort(tab, pierwszy, mid);
	scalanie_sort(tab, mid + 1, ostatni);
	scalanie(tab, pierwszy, mid, ostatni);
}

template<typename typ> void scalanie(TabSort<typ> tab, const int pierwszy,
		const int srodkowy, const int ostatni) {
	int *temp = new int[ostatni - pierwszy + 1];

	int lewy = pierwszy;
	int prawy = srodkowy + 1;
	int biezacy = 0;

	while (lewy <= srodkowy && prawy <= ostatni) {
		if (tab[lewy] <= tab[prawy]) {
			temp[biezacy] = tab[lewy];
			lewy++;
		} else {
			temp[biezacy] = tab[prawy];
			prawy++;
		}
		biezacy++;
	}

	if (lewy > srodkowy) {
		for (int i = prawy; i <= ostatni; i++) {
			temp[biezacy] = tab[i];
			biezacy++;
		}
	} else {
		for (int i = lewy; i <= srodkowy; i++) {
			temp[biezacy] = tab[i];
			biezacy++;
		}
	}

	for (int i = 0; i <= ostatni - pierwszy; i++) {
		tab[i + pierwszy] = temp[i];
	}
	delete[] temp;
}

template<typename typ> void quicksort(TabSort<typ> tab, int pierwszy,
		int ostatni) {
	int lewy = 0;
	int prawy = 0;
	int pivot = 0;
	if (pierwszy >= ostatni)
		return;

	lewy = pierwszy;
	prawy = ostatni;

	pivot = tab[(pierwszy + ostatni) / 2];

	while (lewy <= prawy) {
		while (tab[lewy] < pivot)
			lewy++;
		while (tab[prawy] > pivot)
			prawy--;
		if (lewy <= prawy) {
			typ temp = tab[lewy];
			tab[lewy] = tab[prawy];
			tab[prawy] = temp;
			lewy++;
			prawy--;
		}
	}
	quicksort(tab, pierwszy, prawy);
	quicksort(tab, lewy, ostatni);
}

template<typename typ> void sort_wstaw(TabSort<typ> tab, int ilosc) {
	typ tmp;
	int j;
	for (int i = 1; i < ilosc; i++) {

		tmp = tab[i];
		j = i - 1;

		while (j >= 0 && tab[j] > tmp) {
			tab[j + 1] = tab[j];
			--j;
		}
		tab[j + 1] = tmp;

	}

}

template<typename typ> void sortshell(TabSort<typ> tab, int ilosc) {
	int temp, i, j;
	int pivot = ilosc / 2;

	while (pivot) {
		for (i = pivot; i < ilosc; i++) {
			temp = tab[i];
			j = i;
			while (j >= pivot && tab[j - pivot] > temp) {
				tab[j] = tab[j - pivot];
				j = j - pivot;
			}
			tab[j] = temp;
		}

		pivot = pivot / 2;
	}
}

template<typename typ> void sortheap(TabSort<typ> tab, int ilosc) {
	for (int i = ilosc / 2; i >= 0; i--) {
		zejscie(tab, i, ilosc);
	}

	while (ilosc - 1 > 0) {

		typ temp = tab[ilosc - 1];
		tab[ilosc - 1] = tab[0];
		tab[0] = temp;
		zejscie(tab, 0, ilosc - 1);

		ilosc--;
	}
}

template<typename typ> void zejscie(TabSort<typ> tab, int licznik, int ilosc) {
	while (licznik * 2 + 1 < ilosc) {
		int dol = 2 * licznik + 1;

		if ((dol + 1 < ilosc) && (tab[dol] < tab[dol + 1]))
			dol++;

		if (tab[licznik] < tab[dol]) {
			typ temp = tab[dol];
			tab[dol] = tab[licznik];
			tab[licznik] = temp;
			licznik = dol;
		} else
			return;
	}
}
#endif /* SORT_HPP_ */